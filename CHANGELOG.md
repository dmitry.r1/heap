# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](Https://conventionalcommits.org) for commit guidelines.

<!-- changelog -->


## v3.0.0

* Change attitude for running `Heap.split/2` on empty heap.
    
    Previous version - it returns `{nil, nil}`, and now `nil`.

    Function is suitable to work with `Stream.unfold`, check the documentation for details.

    Now, specs are precise.

* Remove useless atom-based comparators in order to standartise the API.
    One can use `Heap.min/2` and `Heap.max/2` in order to define *default* heap types.

    One can use only anonimous functions in order to define custom comparator. 

* `Heap.new/0` is removed, in order to make heap creation explicit. 
    You can use `Heap.min/0` as old `Heap.new/0` with the same attitide.
    Please, specify the heap type explicitely with `Heap.min/0` or `Heap.max/0`.

## [v2.0.2](https://gitlab.com/jimsy/heap/compare/v2.0.2...v2.0.2) (2020-12-28)
