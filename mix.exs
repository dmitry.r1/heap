defmodule Heap.Mixfile do
  use Mix.Project

  @version "3.0.0"

  def project do
    [
      app: :heap,
      version: @version,
      description: description(),
      elixir: "~> 1.5",
      build_embedded: Mix.env() == :prod,
      start_permanent: Mix.env() == :prod,
      package: package(),
      deps: deps()
    ]
  end

  def application, do: []

  def description, do: "Small composable Heap implementation. Heaps sort elements at insert time."

  def package do
    [
      maintainers: ["James Harton <james@harton.nz>"],
      licenses: ["MIT"],
      links: %{
        "Repository" => "https://gitlab.com/jimsy/heap",
        "Docs" => "https://hexdocs.pm/heap"
      }
    ]
  end

  defp deps do
    [
      {:ex_doc, "> 0.0.0", only: ~w[dev test]a},
      {:earmark, "~> 1.4", only: ~w[dev test]a},
      {:dialyxir, "> 0.0.0", only: ~w[dev test]a},
      {:credo, "~> 1.6", only: ~w[dev test]a, runtime: false},
      {:git_ops, "~> 2.4", only: ~w[dev test]a, runtime: false}
    ]
  end
end
